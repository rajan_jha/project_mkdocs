#Take alpine as the base image
FROM alpine
MAINTAINER rajan_jha
#Install mkdocs and other libraries
RUN \
    apk add --update \
    bash \
    python3 \
    python3-dev \
    build-base  && \
    pip3 install --upgrade pip && \
    pip install mkdocs

RUN mkdir -p /mkdocs
#Copy the wrapper cript to work directory
COPY mkdockerize.sh /mkdocs

WORKDIR /mkdocs
#Entrypoint for wrapper script to take care of arguments

ENTRYPOINT ["./mkdockerize.sh"]

