****Introduction:****

[Docker Image](https://hub.docker.com/repository/docker/rajan405099/project_mkdocs) with [MkDocs](http://www.mkdocs.org/)

It's using a tiny image provided by Alpine.
MkDocs is a fast, simple and downright gorgeous static site generator that's geared towards building project documentation. Documentation source files
are written in Markdown, and configured with a single YAML configuration file.

Purpose of this image was to simplify the process of deploying MkDocs. This image is based on Alpine Linux to minimize
the size of the image.You can customize index.html as per your need to host a static website using this docker image.


**Tools required to run this project :**

1.Docker : You will need your docker hub credentials to login through cli.

$docker login (not specifying a particular registry will log you in docker hub by default)

2.Gitlab : Initialize gitlab account using your credentials and pull the project repository in your home directory,

$cd
$git clone [my_git_repos](https://gitlab.com/rajan_jha/project_mkdocs.git)

$cd project_mkdocs
(Make sure you have the root privileges)

Pull the latest image with master tag by running below command,
$docker run -it rajan405099/project_mkdocs:master
This will download the mkdocs docker image which is build using GITLAB pipeline and pushed to docker registry for public use.
Command will show the usage as we are not providing any argument here.

**Produce :**

This option Passes as an argument and read the local directory which was cloned from gitlab (~/project_mkdocs)and create all
the necessary resources which would be used to serve a static website.

$docker run -v ~/project_mkdocs:/mkdocs rajan405099/project_mkdocs:master produce

Above command will mount the local directory to the work directory inside the container and all the resources including the index.html will be created here. 
Make sure you have run the produce option before serving the website.

**Serve:**

This option will read all the resources created by produce command and then use mkdocs to run the website on your local machine.

$docker run -p 8000:8000 -v ~/project_mkdocs:/mkdocs rajan405099/project_mkdocs:master serve

Once the website is running, it can be accessed using [localhost](http://localhost:8000)

To run the website in interactive mode run,

docker run -it -p 8000:8000 -v ~/project_mkdocs:/mkdocs rajan405099/project_mkdocs:master serve

To run the website as a daemon mode,

docker run -it -d -p 8000:8000 -v ~/project_mkdocs:/mkdocs rajan405099/project_mkdocs:master serve

**CI pipeline**

We are using gitlab for CI build and test the image.You can check the jobs in CI/CD section for more details.
After the build, GITLAB pushes the container to dockerhub with master tag.


**Troubleshooting**

docker ps : To check if the docker is running in daemon mode;
docker ps -a : exited containers;
docker logs container_name: To check the logs of running or exited container

**AUTHOR**

RAJAN JHA


