#!/bin/bash

#usage : will display when no argument is provided
show_usage() {
      usage="usage :
		 MkDocs is a fast, simple and downright gorgeous static site generator. This docker image has mkdocs installed which can be used to produce and serve a static website.\n
		 Please pruduce the website first by following the instructions in readme.md and once produced, website can be accessed at http:localhost:8000 by using serve option.\n\n "
      printf "$usage" >&2
}
if [[ "$1" = "" ]];
then
  show_usage
  exit 0
fi

#if multiple random arguments provided

if [[ "$#" -ne "1" ]]
then echo "Expecting only one argument here either produce or serve........";
exit 1
fi

#if some random arg is provided
if [[ "$1" != "produce" && "$1" != "serve" ]];
then echo "Options are 1.produce or 2.serve....."
exit 1
fi

DIR=my_project
#check if the project dir was created previously
if [[ -d "$DIR" ]];
then rm -rf $DIR;
fi
#producing the website
if [[ "$1" = "produce" ]];
then
mkdocs new $DIR &>/dev/null;
cd $DIR;
cat > mkdocs.yml << EOL
site_name: rajan_jha
nav:
   - index.html
   - about_me.html
EOL
cd docs/;
echo "Hello All !! Hope you are doing well." >> about_me.html
echo "Hello There !! Hope you are doing well. This website is created using mkdocs and the owner of this website is Rajan_Jha." >> index.html
rm -f index.md
cd ../..
tar -zcf $DIR.tar.gz $DIR;
rm -rf $DIR;
exit
fi
#serving the wbeite
if [[ "$1" = "serve" ]];
then
printf "checking if website was correctly produced.....\n\n"
if [[ -f "$DIR.tar.gz" ]];
then
printf "website was produced successfully.....\n\nNow you are ready to serve the website.......\n\n"
printf "serving the website.....\n\n"
tar -xf $DIR.tar.gz && cd $DIR && mkdocs serve --dev-addr=0.0.0.0:8000
cd ..
rm -rf $DIR*
else
printf "project directory is missing.....\n\nplease produce the website and come back.....\n"
exit
fi
fi